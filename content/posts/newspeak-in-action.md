---
title: "Newspeak in Action"
date: 2021-03-28T09:07:55+02:00
draft: false
subtitle: ""
image: ""
tags: [politik, gesellschaft, philosophie]
---

Es ist vielleicht unfair, als studierter Philosoph die Denkfähigkeit eines promovierten Mediziners zu kritisieren.
Das wäre wohl vergleichbar damit, wenn ein Sternekoch das Gericht eines Hobbykocks kritisierte.

Was Kanzleramtsminister Helge Braun [nun](https://www.bild.de/politik/inland/politik-inland/minister-helge-braun-im-interview-kanzleramt-warnt-vor-super-mutante-75884950.bild.html) von sich gegeben hat, verlangt jedoch nach einem Kommentar:

> „Wir haben keine Quote festgelegt. Klar ist, wenn es nicht zwei Drittel bis drei Viertel der Firmen sind, ist es zu wenig“

Nur kurz analysisert.
Zuerst stellt er fest, daß die Regierung keine Quote festgelegt habe.
Dann stellt er fest, daß die Regierung von einem bestimmten Anteil der Firmen etwas erwartet.

Wobei eine Quote nichts anderes bezeichnet als einen Anteil von einem Ganzen; also zum Beispiel: "zwei Drittel" oder "drei Viertel".

Der Herr Braun schafft es also festzustellen, daß ein festgelegter Anteil kein festgelegter Anteil ist.
Ein Beispiel für newspeak wie aus dem Lehrbuch.

Dazu Wilhelm Dilthey an Graf Paul Yorck von Wartenburg 1880:
"Ich habe von neuem den Eindruck gehabt daß mit einem sehr maßvollen und in keiner Weise übertriebenen Quantum
von Intelligenz unser liebes Deutschland, wahrscheinlich die ganze Welt regiert wird."
