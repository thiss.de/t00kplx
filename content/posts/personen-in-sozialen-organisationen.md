---
title: "Personen in Sozialen Organisationen"
date: 2020-02-03T14:05:05+01:00
draft: false
subtitle: ""
image: ""
tags: []
---

Wie sinnig ist es, wenn wir unsere sozialen Kontakte in Nicht-Anwesenheit
ausüben? Direkte Interaktion, die Vielfalt menschlichen Ausdrucks werden auf
Zeichen reduziert, deren Bedeutung in ein intellektuelles System gefaßt ist.
Was der andere dann wirklich meint, was er empfindet ist nur noch in dem Maße
möglich, wie er es in diesen Zeichen fassen kann. Der Austausch mit dem
anderen ist ohne Gegenwart, ohne Erleben und Empfinden.
