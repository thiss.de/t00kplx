---
title: "Wahrnehmungs Irrsinn"
subtitle: ""
date: 2021-11-22T17:27:26+01:00
image: ""
draft: false
tags: []
---

Der Irrsinn, den wir gerade erleben, läßt sich damit zusammenfassen, daß
eine geänderte Wahrnehmungs- und Beobachtungssituation zu einer veränderten
Vorstellung von der Welt führt, obwohl sich die Welt in ihren
Daseinszusammenhängen nicht verändert hat.
