---
title: "Woher der Anspruch?"
subtitle: ""
date: 2021-04-12T13:42:27+02:00
image: ""
draft: false
tags: [gesellschaft, politik]
---

Wie begründet sich der Anspruch von anderen Menschen ein bestimmtes Verhalten der Anpassung an die Welt zu fordern?

Es ist ein beständiges Narrativ der Neuzeit, das seit dem 19. Jahrhundert das Leben der meisten Menschen bestimmt, 
daß der Mensch lernen müsse die gemeinschaftliche Art des Umgangs mit der Welt zu reproduzieren.

Aber es gibt keine Einsicht in den Grund, warum diese Forderung angemessen wäre.

Sind wir nicht alle als Freie geboren? Als Wesen, die aus eigener Einsicht und Anschauung entscheiden können.
Als Wesen, die sich selber orientieren können in ihrer Welt und entscheiden dürfen, was ihnen wichtig und bedeutsam.
Sind das nicht die Versprechen der Religionsfreiheit, der Trennung von Kirche und Staat?

Haben wir nicht die Zeit hinter uns gelassen, in der eine kleine Gruppe über eine 
größere Gruppe herrscht? 

Haben wir nicht das Prinzip des demokratischen Rechtsstaates eingeführt, um genau dies zu verhindern?

Auf welcher Basis maßen sich Menschen an, anderen vorschreiben zu können, wie sich an das Leben anzupassen haben?
