---
title: "Warum Denken?"
subtitle: ""
date: 2021-04-18T05:16:05+02:00
image: ""
draft: false
tags: [philosophie]
---

Warum fällt es den Menschen so schwer, sich eine Sache zuerst einmal selber anzuschauen und zu durchdenken?

Stattdessen rufen sie ständig nach einem Experten, dessen Meinung sie dann wiederholen können.

Aber darum geht es beim Menschen nicht.

Jeder Mensch muß für sein eigenes Leben einstehen, was er selber nicht erkennt, nicht selber begreift, kann ihm auch nicht wirklich 
weiterhelfen.

Denken heißt die Zusammenhänge der wahrgenommenen Wirklichkeit zu einem einheitlichen Bild zusammenzufügen.

In einem wunderbaren Vergleich, hat Egon Friedell festgestellt [^1], daß Weltanschauung es dem Menschen ermöglicht, wie eine Katze
aus großer Höhe zur Erde zu fallen und dabei doch auf allen Vieren sicher zu landen.

[^1]: Egon Friedell, Kulturgeschichte der Neuzeit, München 1931
