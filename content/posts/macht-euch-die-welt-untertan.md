---
title: "Macht Euch Die Welt Untertan"
subtitle: ""
date: 2021-04-12T12:11:49+02:00
image: ""
draft: true
tags: []
---

Bibel: macht euch die Welt Untertan.

> Quellenzitat einfügen

In der Buber-Rosenzweig übersetzt heißt es

> Gott sprach:
> Die Erde treibe lebendes Wesen nach seiner Art, Herdentier, Kriechgerege und das Wildlebende des Erdlands nach seiner Art!
> Es ward so. 
> Gott machte das Wildlebende des Erdlands nach seiner Art und das Herdentier nach seiner Art und alles Gerege des Ackers nach seiner Art.
> Gott sah, daß es gut ist.
> Gott sprach:
> Machen wir den Menschen in unserem Bild nach unserem Gleichnis!
> Sie sollen schalten über das Fischvolk des Meeres, den Vogel des Himmels, das Getier, die Erde all, und alles Gerege, das auf Erden sich regt.
> Gott schuf den Menschen in seinem Bilde,
> im Bilde Gottes schuf er ihn,
> männlich, weiblich schuf er sie.
> Gott segnete sie,
> Gott sprach zu ihnen:
> Fruchtet und mehrt euch und füllet die Erde und bemächtigt euch ihrer!
> schaltet über das Fischvolk des Meeres, den Vogel des Himmels und alles Lebendige, das auf Erden sich regt!
> Gott sprach:
> Da gebe ich euch 
> alles samensäende Kraut, das auf dem Antlitz der Erde all ist,
> und alljeden Baum, daran samensäende Baumfrucht ist,
> euch sei es zum Essen,
> und allem Lebendigen der Erde, allem Vogel des Himmels, allem was auf Erden sich regt, darin lebendes Wesen ist,
> alles Grün des Krauts zum Essen.
> Es ward so.
> Gott sah alles was er gemacht hatte,
> und da, es war sehr gut.
> Abend ward und Morgen ward: der sechste Tag.

Unterschied: Macht, Untertan machen.

Verantworung des 
