---
title: "Der Wolf und die sieben Geisslein"
subtitle: ""
date: 2021-05-15T21:07:20+02:00
image: ""
draft: false
tags: [maerchen]
---

Das Märchen vom [Wolf und den sieben Geißlein](https://kurzemaerchen.de/maerchen/fuer-kinder/der-wolf-und-die-sieben-geißlein/) 
liefert ein Beispiel für fehlgeleitete Orientierung.

Die Mutter meint das Böse anhand von Merkmalen erkennen zu können.

Hätte sie das Prinzip des Bösen erklärt, die Kinder wären nicht Opfer geworden.
