---
title: "Maske Auf, Fresse Halten"
date: 2021-03-21T06:20:59+01:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft]
---

Vergangene Woche habe ich eine sogenannte politische Open-Mic Veranstaltung beobachtet.

Dort hat eine Ärztin darüber berichtet, wie sozialen und caritativen Einrichtungen die
finanziellen Mittel gekürzt werden. Hilfsbedürftige Menschen erhalten in der Konsequenz
keine persönliche Hilfe mehr. Stattdessen werden diese Menschen mit Medikamenten ruhig
gestellt. Der Auffassung dieser Ärztin nach, werden kranke Menschen ungeheilt entlassen.

Dieses Geschehen wird kaum in der öffentlichen Berichterstattung erwähnt.

Am Rande dieser Veranstaltung war eine Gegendemonstrantin, die mit einem Schild ihre Meinung
zum Ausdruck brachte: "Maske auf, Fresse halten".

Offensichtlich gefällt es ihr nicht, daß die unsolidarischen Nebeneffekte des "solidarischen"
Lockdowns im öffentlichen Raum zur Sprache gebracht werden.
