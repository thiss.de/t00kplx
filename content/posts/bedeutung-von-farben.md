---
title: "Bedeutung von Farben"
subtitle: ""
date: 2021-03-14T17:42:48+01:00
image: ""
draft: true
tags: [gesellschaft]
---

Eher politisch linksgerichtete Personen, vertreten die Auffassung, daß der Farbe keine Bedeutung zukommt.
Dieser Auffassung möchte ich voll und ganz zustimmen und nur auf eine Beobachtung hinweisen, die mir gerade
bei linksgerichteten Menschen auffällt.

Diese scheinen sich gerne schwarz zu kleiden, insbesondere wenn sie eine Demonstration veranstalten, so daß
in den Zeitungen öfter vom "schwarzen Block" berichtet wird.

Nachdem ich in letzter Zeit aufgrund der Teilnahme an Veranstaltungen zur politischen Meinungsbildung teilgenommen
habe, ist mir die unterschiedliche Farbgebung der Beamten der Exekutive aufgefallen.

Die eher zupackenden Vertreter der Bereitschaftspolizei tragen schwarze Uniformen. Damit sind sie durchaus geeignet einen
bedrohlichen Eindruck hervorzurufen. Zur Deeskalation einer Demonstration wäre eine freundliche Farbe sicher besser
geeignet, wie zum Beispiel ein freundliches Altrosa. So ein sanftes Rosa, daß gleich das Gemüt beruhigt.
