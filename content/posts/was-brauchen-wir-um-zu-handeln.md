---
title: "Was brauchen wir um zu Handeln"
subtitle: ""
date: 2021-04-23T08:19:10+02:00
image: ""
draft: false
tags: [philosophie]
---

Was brauchen wir, um lähmende Schwere und Orientierungslosigkeit zu überwinden und zu einem gerichteten Handlungsimpuls zu kommen?

Wie kommen wir zu einem klaren, auf Zugang zur Welt ausgerichtetem Verständnis
für die Verschieden- und Anderartigkeit dessen, was uns begegnet?

Wie kommen wir zum Handeln in der Begrenzung unseres konkreten Daseins, im Wirkungsbereich der faßbaren Lebenswirklichkeit,
innerhalb der Grenzen unseres Ortes?

Was brauchen wir, daß wir dort und dann handeln, wo wir leben?

Nicht die Flucht antreten in Lebensbereiche und Lebenswirklichkeiten, die nicht die unseren sind, sondern die anderen Menschen 
zugehören.

Wie vermeiden wir die Flucht in fremdes Leben? 

Die Flucht in den Übergriff, wo wir dann andere Menschen statt unser selbst handeln lassen.

Eine Antwort auf diese Frage, kann jeder Mensch nur in sich wachsen lassen.

Eine fremde Antwort ist jeweilig immer nicht innerhalb der Grenzen der eigenen Lebenswirklichkeit gewachsen und kann damit nicht 
zu einem eigenen Handeln führen.
