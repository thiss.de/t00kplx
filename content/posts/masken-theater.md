---
title: "Masken Theater"
subtitle: ""
date: 2021-03-19T08:13:48+01:00
image: ""
draft: false
tags: [gesellschaft]
---

Man könnte mir natürlich vorwerfen, daß ich den Ernst der Lage ignoriere oder
einfach nicht erkenne. Solche Vorwürfe muß man ertragen können, wenn man sich eine
eigene Meinung leisten will. Also sei's drum.

Bei der ganzen Diskussion um die Maskenpflicht im öffentlichen Raum wird ausgeblendet,
daß die Menschen der modernen Welt ohnehin die ganze Zeit mit einer Maske rumgelaufen sind.
Nicht erst seit Corona.

Es war kaum ein ehrliches Gesicht zu sehen. Die Menschen wollten doch ihr eigentliches Gesicht
verstecken, hinter den Zeichen und Symbolen des In-Seins, des Up-to-dates-Seins. Da hatten sich
ihre äußeren Zeichen in Form von Markenkleidung, bestimmten Kombinationen von Farben und Accessoires
(man denke Tücher eines bestimmten Designs oder der Bevorzugung bestimmter Farben).

Da ist die aktuelle Pflicht zur Vermummung nur ein obrigkeitlich verordnetes Zeichen für den
schon seit langem bestehenden inneren Zustand der Gesellschaft und der Menschen.
