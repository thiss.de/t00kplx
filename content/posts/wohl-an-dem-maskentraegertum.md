---
title: "Wohl an Dem Maskenträgertum"
date: 2021-03-24T09:00:37+01:00
draft: false
author: "Sita"
subtitle: ""
image: ""
tags: [erzaehlungen]
---

Wenn ich morgens eine neue Maske aus der Cellophanverpackung schäle, knistert es verheißungsvoll.

Behutsam, nicht die Stoffseiten mit den Fingern zu berühren, fasse ich sie an den Trägern an und führe sie vor mein Gesicht. Oft stehe ich dabei am Spiegel im Flur, schaue mir selbst tief in die Augen während mein Gesicht unter ihnen verschwindet.
Die Maske ruht weich und warm auf der Haut meines Gesichts, sanft auf Wangen, Nase und über dem Mund.

Nun bin ich frei, wenn ich auf die Straße trete.

Die Luft, die ich atme, ist nicht die kalte Luft des Morgens, die Luft, die ich atme, ist von meinen warmen Atemzügen angewärmt, ich rieche den Duft meiner Zahnpasta und fahre nochmals mit der Zunge über meine frisch geputzten Zähne.
Unter den Menschen, die mir begegnen, haben nur wenige ihr Gesicht bedeckt. Diese verachte ich insgeheim für ihre Schwäche. Sie sagen das Tragen einer Maske wäre schädlich, sie würden keine Luft mehr bekommen und dergleichen mehr. Dieser freie Atem ist mir nicht erstrebenswert, ich brauche ihn nicht, denn frei bin ich nicht, wenn ich frische Luft in meine Lungen ziehe, frei bin ich, wenn ich weiß, daß ich das Richtige tue. Und von dem Richtigen, das ich tue bin ich zutiefst überzeugt.

Mit den Masken schützen wir uns gegenseitig.
Wir schützen uns voreinander, wenn wir uns fernbleiben. Hätten wir das in früheren Zeiten geahnt, wie viel Leid und Krankheit hätten wir verhindern können!

Ich verzichte nun auf diese unnötigen Kontakte, ich frage niemand nach dem Weg, denn ich gehe keine fremden Wege. Fragt mich jemand etwas oder kommt mir jemand zu nah, springe ich schnell einen Schritt zurück. Die Menschen müssen es lernen sich zu kontrollieren.
Die Zeiten des wilden Getummels sind vorbei. Wenn wir zueinander kommen, dann aus der Unvermeidbarkeit der Situation, wenn ein Treffen online keine Abhilfe schafft.

Ich begreife nicht, wie man derart rücksichtslos miteinander umgehen kann, eine Person aus einem anderen Haushalt in den Arm zu nehmen. Damit gefährdet man nicht nur sich, sondern ganze Familien und Betriebe, die wegen folgendenden Krankheitsfällen schließen müssen.

Insgesamt kenne ich das Bedürfnis nach Nähe nicht – seit der frühesten Kindheit habe ich meine Altersgenossen mit Mißtrauen in ihren Spielen beobachtet. Immer hatte ich ihnen vom Rand aus zugesehen und ob des kümmerlichen Ausdrucks in meinem Gesicht Mitleid erregt, und so zuweilen ins Spiel dazukommen dürfen. Nicht selten war dies auf Ermahnung der Erwachsenen geschehen, denn im Spiel war ich nicht beliebt, ich war unbeholfen und warf nicht selten etwas um oder zerstörte aus Ungeschicklichkeit die mühsam gebauten Burgen und Kunstwerke der anderen Kinder.

Hätte ich damals bereits eine Maske tragen dürfen, wäre vieles leichter gewesen.

Niemand mehr hätte den leidenden Zug um meinen Mund gesehen, der von dem störrischen Ingrimm der Kinderaugen überschattet worden wäre. Die anderen Kinder, sie hätten nicht miteinander spielen dürfen und es hätte die Spiele an deren Rand ich stand, nicht gegeben.

Doch jetzt bin ich Teil einer großen Gemeinschaft, die sich großes zu tun aufgeschwungen hat.

Die wahre Gleichheit unter den Menschen hervorzubringen und den wahren Sieg über alls was uns angreift davonzutragen, das ist unser Ziel.
Und wir werden es denen zum Trotz erreichen, die uns zu schaden versuchen, denen, die meinen, sie wollten selbst leben, denn diese Zeiten des Lebens sind vorbei.
Wir können es uns nicht mehr leisten als einzelne zu handeln, zu denken oder gar zu empfinden.

Erst wenn wir in der großen Gemeinschaft aufgehen, werden wir all unsere Probleme zu lösen in der Lage sein. Erst wenn der Einzelne sich in den Dienst an dem großen der Gemeinschaft stellt, werden wir erlöst sein.

All jene, die sich gegen uns stellen, all jene, die uns nicht folgen, denn was ist das nicht folgen anderes als eine verkleidete Gegenwehr, werden wir vernichten zu streben haben.
Das tun wir nicht gern, doch lassen sie uns keine Wahl, denn was wir im Sinn haben, ist die Freiheit aller, an der der Einzelne aufgehen oder verenden muß.
Die gewonnene Freiheit ist eine neue Freiheit sich endgülten aufgeben zu dürfen, sie gibt mir Frieden und das Gefühl für die Ordnung in die alles für uns gebracht wurde.
Und dafür bin ich dankbar.

Sie sollte nicht mehr lange leben, nicht wenige Tage darauf wurde sie beim Überqueren einer Kreuzung von einem Lastwagen erfasst und gegen einen Brückenpfeiler geschleudert. In den Statistiken wird sie geführt als eine an einer neuartigen, tödlichen Krankheit verstorbene.

An dieser Stelle möchte ich mein herzliches Beileid an all ihre Bekannten und Freunde und an erster Stelle ihrer Familie aussprechen, die ihren Tode wohl sehr bedauern werden, haben sie sie doch seit über einem Jahr nicht mehr zu Gesicht bekommen.
So schwinden manche Menschen von den Lebenden bevor ihre Zeit zu sterben gekommen ist.
