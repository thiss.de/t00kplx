---
title: "Was Für Zeiten?"
subtitle: ""
date: 2021-04-15T07:17:22+02:00
image: ""
draft: false
tags: [gesellschaft, politik]
---

In was für Zeiten leben wir eigentlich? 

Hat es das jemals gegeben, daß im Namen der Gesundheit alle Macht in Deutschland zentralisiert wurde?

Daß Zwang ausgeübt wurde, um die Entscheidungen der Politik durchzusetzen? 
(Man denke an die Durchsuchungen von Arztpraxen, die an Promenaden den Bürger zum Regeleinhalten auffordernden Polizeihorden)

Daß die exekutive Gewalt mit brutalen Methoden gegen den Bürger vorgeht?
(Wie die brutalen Festnahmen durch schwarzgekleidete Polizisten von Anwälten und Pensionären)

Daß Andersdenkende diffamiert und aus dem öffentlichen Diskurs ausgeschlossen werden?
(Beispiele hierfür tauchen in aller Regelmäßigkeit in den Medien auf. Herausgegriffen seien John Ioannidis, Sucharit Bhakdi, Wolfgang Wodarg)

Daß die Regierung so von ihrem Handeln überzeugt ist, daß sie es für alternativlos hält, keinerlei Fehler an ihren Entscheidungen 
erkennen kann und eigentlich nur Lob für die eigenen Entscheidungen übrig hat?

Daß, wenn Fehler eingestanden werden, sie immer durch die gute Absicht gerechtfertigt werden? 
Durch die Beschwörung des angeblich Guten, das sie bewirken? 
Getreu dem Motto, man könne doch nicht das Licht ausblasen, weil einem der Schatten störe.

Wann sind sie nicht mehr nur die Träger von Informationen der Medien, der Wissenschaft, des Staates?

Wann hören die Menschen endlich auf ihr eigenes Urteil? 
