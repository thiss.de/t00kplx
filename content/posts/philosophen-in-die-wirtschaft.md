---
title: "Philosophen in Die Wirtschaft"
date: 2019-12-18T20:18:07+01:00
draft: true
subtitle: ""
image: ""
tags: []
---

Meine Alma-Mater hat auf [twitter](https://twitter.com/hfph_muc/status/1204053086765027328) veröffentlicht,
daß Philosophen in großen IT-Firmen arbeiten sollten.

Der verlinkte Artikel scheint ja sehr interessant zu sein, aber leider kommt die ganze Aktion
ungefähr 20 Jahre zu spät. Mittlerweile hat die Generation studiert, die mit Informationstechnologie
großgeworden ist, deren ganzes Denken von einer Welt bestimmt ist
in der jegliche Gegenwart von Informationen und Erfindungen besetzt ist, wo die Ruhe und die Zeit
für das Wachsen einer eigenen Anschauung nicht mehr gegeben ist.

Wäre zumindest die Gelegenheit Ernst Cassirer zu lesen und viel Spaß beim Dekodieren von Informatik zu wünschen.
