---
title: "Ansprüche aus dem Jahre 1912"
date: 2021-04-12T08:20:08+02:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft, bildung]
---

In einem Lehrbuch der ebenen Geometrie <sup id="s1">[1](#vorwortbuch)</sup> 
aus dem Jahre 1912 habe ich im Vorwort den Hinweis
gefunden, daß die Kenntnis der unterschiedlichen Rechenanweisungen nicht 
die Einsicht in die inneren Zusammenhänge vermittle und darum die Frage 
nach dem "Warum" nicht beantwortet werde.

![Vorwort](images/vorwort.jpeg)

Mir scheint das in den Jahren seit erscheinen dieses Lehrbuches der 
Anspruch etwas gesunken ist. Eine innere Einsicht oder die Frage nach dem 
"Warum" wagt heute niemand mehr zu fordern. Schöne neue Welt.

<a id="vorwortbuch" name="vorwortbuch">1</a>: G. Recknagel & G. Wetzstein,
Lehrbuch der ebenen Geometrie, Nürnberg 1912 [↩](#s1)
