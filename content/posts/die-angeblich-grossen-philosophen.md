---
title: "Die \"großen\" Philosophen"
date: 2021-04-19T15:06:32+02:00
draft: false
tags: [philosophie]
---

Ein Treppenwitz der Philosophiegeschichte sind für mich Kant, Hegel und Marx.

Kant, weil er seine an der Newton'schen Mechanik geschulte Vernunft, für die einzig mögliche Art und Weise des Verarbeitens von 
Sinneseindrücken hielt.

Hegel, weil er mit "Was vernünftig ist, das ist wirklich; und was wirklich ist, das ist vernünftig" den blödesten Satz der abendländischen
Philosophiegeschichte hervor gebracht hat.

Marx, weil er mit seinem begrenzten Anschauungshorizont, die wesentlichen Entwicklungen der Neuzeit aus dem öffentlichen Bewußtsein 
ausgeschlossen hat.
