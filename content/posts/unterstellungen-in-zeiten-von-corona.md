---
title: "Unterstellungen in Zeiten von Corona"
date: 2021-03-31T06:43:19+02:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft, erkenntnistheorie]
---

Die [Welt](https://www.welt.de/wirtschaft/article229450905/Deutsche-Krankenhaeuser-Wird-Corona-jetzt-wichtiger-als-Krebs.html)
hat einen Artikel veröffentlicht, der eine langgehegte Unterstellung enthält, die seit Beginn der sogenannten Corona-Krise
ihr Unwesen in der Berichterstattung treibt.

Die Apologeten dieser Unterstellung kommen aus der Medizin, Wissenschaft, Politik und sie alle scheinen sich nicht im geringsten
darüber im Klaren zu sein, daß diese Unterstellung nichts anderes ist, als eine subjektive Vermutung, die ohne Bezug auf
Beobachtungs- oder Erfahrungstatsachen, einfach behauptet wird.

Hier der entsprechende Auszug:

> Studien deuten daraufhin, dass vor allem schwere Notfälle im Krankenhaus landen. In den übrigen Fällen sind das Misstrauen der Patienten in das Gesundheitssystem und die Angst, sich mit Corona zu infizieren, offenbar so hoch, dass einige Menschen trotz eindeutiger Beschwerden eine Behandlung versuchen zu vermeiden.

Da wird ohne irgendeine Studie zu benennen festgestellt, die Menschen gingen nicht ins Krankenhaus, weil sie Angst hätten sich
mit Corona zu infizieren.

Als ob die Angst vor einer Coronainfektion tief verwurzelt in der Bevölkerung sei.
Die gleiche Behauptung wurde auch schon für den öffentlichen Personennahverkehr herangezogen oder für andere Bereiche des
öffentlichen Lebens, die durch ein strenges Hygieneregime geregelt werden und bei denen die Menschen einfach nicht mitziehen.

Wenn die Angst vor Corona in der Bevölkerung so tief verwurzelt wäre, wie dort unterstellt, dann würden sich die regelmäßig
anzufindenden Menschenansammlungen nicht finden lassen. Seien es überfüllte Bergstationen, Parks, Märkte.

Dort haben die Menschen keine Angst vor einer Infektion, sondern lediglich vor der Polizei die "wieder Druck" machen will
(wie unlängst ein Einsatzleiter seinen Unterstellten erklärt hatte, was ihre Aufgabe sei).

Dabei erwarten den Patienten andere unangenehme Bedrohungen im Krankenhaus.

Zumindest mich würde aktuell nicht die Angst vor einer Coronainfektion vom Krankenhaus fernhalten.
Vielmehr die Angst vor Isolation und Quarantäne, wochenlang in einem Zimmer zu liegen, ohne ein Lächeln oder ein vertrautes Gesicht
sehen zu können. Hernach würde ich so sterben müssen. Dann doch lieber kürzer leben, als unter derartig industriellen Bedingungen
lediglich weiter zu stoffwechseln.
