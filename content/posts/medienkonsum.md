---
title: "Medienkonsum"
subtitle: ""
date: 2021-04-15T07:14:39+02:00
image: ""
draft: false
tags: [leben, philosophie]
---

Bei aller Freude an der Neugier und dem Interesse am Weltgeschehen, muß ich doch feststellen, 
daß der Medienkonsum im höchsten Maße geeignet ist, das Aufkommen eigener Gedanken zu unterbinden.
