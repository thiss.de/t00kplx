---
title: "Die Geistige Übung des Programmierens"
date: 2021-04-10T17:21:16+02:00
draft: true
tags: [programmieren]
---

Eine der am stärksten geforderten Eigenschaften eines Softwareentwicklers ist die Fähigkeit Programmcode lesen zu können.

Die Bereitschaft ein Computerprogramm, das vielleicht erstmal völlig unverständlich erscheint in seiner besonderen Implementierung 
zu verstehen. Und dies völlig unabhängig vom eigenen Ego, daß vielleicht lieber alles anders und besser gemacht haben würde.
