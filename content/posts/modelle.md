---
title: "Modelle"
subtitle: ""
date: 2021-05-11T07:53:11+02:00
image: ""
draft: false
tags: [philosophie]
---

Modelle sind nicht das Leben. 

Sie sind nicht Wirklichkeit, sondern Abbilder der Wirklichkeit.

Abbilder der Wirklichkeit sind Bilder des Wirklichen, die dem Bewußtsein haften geblieben sind.

Modelle werden vom Willen aus diesen Abbildern geschaffen.
