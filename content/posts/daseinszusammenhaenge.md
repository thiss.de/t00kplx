---
title: "Daseinszusammenhänge"
date: 2021-04-12T07:10:10+02:00
draft: false
tags: [philosophie]
---

Betrachten wir die Welt in ihren Zusammenhängen, macht es keinen Sinn, sich vorschnell auf Beobachtungen zu beschränken, die 
aktuell greifbar sind. Vieles was aktuell greifbar ist, hat seine Herkunft woanders und ist auch nicht nur einfach da, sondern 
hat ein Werden.

Dies zeigt sich in den alltäglichsten Dingen. Die Nahrung, die wir zu uns nehmen, gelangt von anderen Teilen der Region oder der 
Welt zu uns. Der Kaffee, den ich gerade trinke ist in subtropischen Regionen der Welt gewachsen. Der Computer, auf dem ich dies 
schreibe, oder auf dem dies gelesen werden kann, wurde in Amerika entworfen und in Asien gebaut.
Neben dieser räumlichen Trennung kommt eine zeitliche hinzu. Viele der Ideen mit der Welt umzugehen, haben ihren 
Ursprung in vergangenen Zeiten.

Sollen die Zusammenhänge von Dingen erfaßt werden, kann es nicht ausreichen, nur das aktuell Greifbare zum Gegenstand der 
Anschauung zu machen. Es scheint vielmehr geboten, nach der räumlichen und zeitlichen Herkunft der Dinge zu schauen, die uns begegnen.

Es kann auch nicht ausreichen, Erklärungen von Zusammenhängen zu übernehmen. Denn diese Erklärungen unterliegen auch immer den 
Einflüssen von Zusammenhängen des jeweilig Erklärenden. Selbst eine Gruppe unterliegt derartigen Einflüssen, werden doch 
Erkärungsmuster im sozialen Austausch übernommen.

Hier scheint es eine absolute Grenze des Erkenn- und Erklärbaren zu geben, die im Menschen begründet ist und die es unmöglich macht, 
eine für alle Menschen verbindliche Beschreibung der Welt zu finden.

Um nun einen Umgang mit der Welt zu finden kann es hilfreich sein diesen Erfahrungswert anzunehmen und innerhalb der Grenzen des 
durch einen selbst Erklärbaren und Beobachtbaren in die Welt zu treten. Ohne Forderungen an die Welt zu stellen, sie habe anders 
zu sein.

Dann werden wir uns aber auch die Zeit nehmen müssen, den Dingen auf den Grund zu gehen und immer wieder zu hinterfragen, ob 
wir auf eine Wahrheit gestoßen sind, die die greifbaren Zusammenhänge des Daseins erkennbar macht, oder ob wir uns ein Bild von der
Welt gemacht haben, daß herausgerissen aus seiner Herkunft und seinem Werden. 
Ob wir lediglich ein Symptom erkennen, oder die Ursache eines Gewordenen.

Dabei werden wir immer wieder auf Fragen stoßen, die wir nur mit "Ich weiß es nicht" beantworten können. 
Das Nichtwissen begleitet menschliches Denken und Fragen stetig.

Stellt man die Frage nach dem Guten, oder die Frage, ob es richtig sei, was man tue, drängt sich das Nichtwissen besonders stark auf.
Meistens wird es dann durch Glauben ersetzt, der sich aus unmittelbaren Beobachtungs- und Erlebenszusammenhängen begründet.
Seit der Neuzeit ist dies durch die auf Francis Bacon zurückführbare Haltung geprägt, das Wahrheit vor allem nützlich zu sein habe.

Dieses Nützliche ist auf die Erlebniswelt eines Menschen beschränkt und nicht geeignet, sich der Wirklichkeit der Dinge zu nähern.

Kommen wir zurück zu einem konkreten Phänomen. Ein Mobiltelefon mit Internetzugang, die sogenannten "Smartphones".
Betrachten wir es aus der rein subjektive erlebbaren Perspektive, können wir es für gut halten. 
Es ist möglich sich ständig Information abzurufen über das Weltgeschehen, man kann Informationen mit anderen Menschen, auch lieben 
Menschen und guten Freunden austauschen, sich mit einem kleinen Spiel für einen Moment von den Sorgen und dem Druck des Alltags ablenken.
Fragen wir jedoch nach den Zusammenhängen die zum Werden dieses Geräts geführt haben, können Zweifel aufkommen. 
Die benötigten Materialien zur Herstellung werden unter erbärmlichen Arbeitsbedingungen aus der Erde geholt, die Arbeiter in den 
Fabriken werden so schlecht bezahlt, daß sie sich die Geräte, die sie produzieren, nicht selber kaufen können, der Strom, der zum Betrieb 
notwendig ist führt zu Umweltzerstörungen und Gesundheitsschäden.

Meistens können wir damit nur umgehen, indem wir diese Zusammenhänge ausblenden.

Wenn wir aber diese Zusammenhänge ausblenden, können wir die Frage nach dem Guten und Richtigen nicht beantworten.

Wir negieren dann die Wesenheit der Welt.
