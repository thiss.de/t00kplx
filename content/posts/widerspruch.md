---
title: "Widerspruch"
subtitle: ""
date: 2021-04-12T18:49:12+02:00
image: ""
draft: false
tags: [gesellschaft, verantwortung]
---

Ricarda Huch erklärt in einem Brief vom 9. April 1933 ihren Austritt aus der Preußischen Akademie der Künste mit folgendem Wortlaut:

> Was die jetzige Regierung als nationale Gesinnung vorschreibt ist nicht mein Deutschtum.\
> Die Zentralisierung, den Zwang, die brutalen Methoden, die Diffamierung Andersdenkender, das prahlerische Selbstlob halte ich für 
> undeutsch und unheilvoll.
> Bei einer so sehr von der staatlich vorgeschriebenen Meinung abweichenden Auffassung halte ich es für unmöglich, in einer staatlichen 
> Akademie zu bleiben.

Wären mehr Menschen so mutig gewesen aus eigener Einsicht zu handeln und konsequent ihrem eigenem Urteil zu folgen, wäre dann den 
Menschen viel Leid erspart worden?
