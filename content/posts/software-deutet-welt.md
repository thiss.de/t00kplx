---
title: "Software Deutet Welt"
date: 2019-12-17T13:16:16+01:00
draft: true
subtitle: ""
image: ""
tags: []
---

Jedes Computerprogramm als Software löst eine spezifische Aufgabe in wiederholbarer Weise.
Die Auswahl der zu lösenden Aufgabe unterliegt dabei dem Erschaffenden der Software. 

Damit entsteht Software aus einer gemeinschaftlichen Übereinstimmung, 
welche Aufgabe als relevant erachtet werden soll und bringt diese zu einer wiederholbaren 
Ausuübung.
