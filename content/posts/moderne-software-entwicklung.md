---
title: "Moderne Software Entwicklung"
date: 2019-12-17T10:01:46+01:00
draft: true
subtitle: ""
image: ""
tags: []
---

Ich weiß nicht mehr genau, wo ich das gelesen habe, jedenfalls meinte irgendein
Manager einer Software-Company, daß es heutzutage für Softwareentwickler wichtiger 
sein, in der Lage zu sein verschiedene Tools zusammenzustückeln, als in der Lage 
zu sein, einen Algorithmus ordentlich zu programmieren.

Dabei bezog er sich auf die Herausforderung beim Entwickeln von Unternehmens-Software,
die ja vor allem auch betrieben werden muß und sehr engen Anforderungen an den 
Entwicklungsprozeß unterliegt, wo dann die zu verwendenden Sprachen vorgegeben sind, die 
Frameworks und bereits bestehende Software eingebunden werden muß.

Jedenfalls kann ich das auch so bestätigen, daß der Trend in diese Richtung geht.

Da ist es dann wichtiger AWS- und Azure Services, deployen von Anwendungen mit
Docker und Kubernetes, Netzwerkkonfiguration, DNS, Routing, und alles mögliche zu kennen, 
was zum Verteilen einer Anwendung nötig ist.

Aber eine klare Idee von einem Programm, was es tun und wie es benutzt werden soll,
das wird gar nicht mehr verlangt, es wird davon ausgegangen, daß die verwendeten
Teile das alles schon beherrschen.

Wenn dann irgendein Problem auftaucht, werden irgendwelche Lösungen aus dem Internet
zusammengesucht, in der Hoffnung, das das ganze anschließend irgendwie funktioniert.

Eine Verständnis für die Funktionsweise, ein wirkliches Begreifen findet nicht mehr statt
und deshalb ist das meiste auch unbenutzbar und eine riesige Verschwendung von Geld und 
Lebenszeit.
