---
title: "Eine Grenze der \"Wissenschaft\""
subtitle: ""
date: 2021-04-25T09:45:09+02:00
image: ""
draft: false
tags: [gesellschaft, philosophie]
---

Wissenschaftliche Methodik sieht unter anderem die Wiederholbarkeit des "Erkenntnisvorgangs" vor.

Somit kann sich "Wissenschaft" nur mit wiederholbaren Vorgängen befassen.

Einmaliges Geschehen, individuelles Erleben wird niemals zu den Fragestellungen der "Wissenschaft" gehören.

Wenn der Mensch ein Individuum ist, wenn jeder Mensch ein einmaliges Wesen ist, das seinen eigenen Umgang mit der Welt aus sich 
erschaffen kann und soll, dann kann die Wissenschaft dabei keine Rolle übernehmen.

Wissenschaft reduziert den Menschen auf einen sich stetig wiederholenden Vorgang.
