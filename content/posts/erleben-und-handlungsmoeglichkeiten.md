---
title: "Erleben und Handlungsmöglichkeiten"
subtitle: ""
date: 2021-04-23T08:19:41+02:00
image: ""
draft: false
tags: [philosophie, soziologie]
---

Die Handlungsmöglichkeiten eines Menschen, ergeben sich aus seinem Erleben.

Die Orientierung in der Umwelt, läßt manche Handlungen als geboten erscheinen, 
andere als verboten.

Wenn das Erleben keinen Gegenwartsbezug hat, verliert der Mensch seinen eigenen Bezug zur Umwelt.

Dann lebt er aus der Übertragung, er ergibt sich dem Übergriff und gibt seine Freiheit auf.

Seine Freiheit, aus eigenem Erleben eine eigene Handlung, ein eigenes Werk, ein eigenes Scheitern 
hervorzubringen.
