---
title: "Katalog Welt"
subtitle: ""
date: 2021-09-25T21:23:45+02:00
image: ""
draft: false
tags: [gesellschaft, soziologie]
---

Manche Menschen scheinen mir zu glauben, sie könnten sich ein Leben wie aus
einem Katalog zusammenstellen.

Aus einer Sammlung von Lebensversatzstücken, die man sich einfach nur aneignen
müsse, die man einfach nur machen müsse, meinen sie ein erfülltes und ganzes
Leben zu haben.

Da holt man sich die eine Anregung hier, die andere dort und immer ist es doch
nur etwas ihnen Fremdes, das sie einfach nur wiederholen.

Sie scheinen mir auch zu glauben, sie könnten das Leben lernen. Dabei beruht
doch alles, was man lernen kann erstmal auf der Beobachtung von gewesenem.

Wer also sein Leben erlernen will, um es leben zu können, wird immer nur ein
fremdes Leben nachvollziehen, da seine individuellen Eigenschaften niemals
vorher unter diesen gegebenen Umständen verwirklicht worden sind.

Das macht den Zauber der Künstler aus: denn jeder kann lernen ein Stück von
Johann Sebastian Bach auf dem Klavier zu spielen, aber für jeden wird dieses
Stück eine besondere Bedeutung haben.

Damit wird das Erleben des Einzelnen zur Quelle seiner Welt und dessen, was die
Welt für ihn bedeutet.

Sofern er als Mensch seine Fähigkeiten entfalten möchte, kommt er nicht umhin,
diesen Schritt ins Leben zu gehen.
