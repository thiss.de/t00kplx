---
title: "Umfragen statt Volksabstimmung"
date: 2021-04-05T09:26:03+02:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft, politik]
---

Es ist in Deutschland etabliert, daß Volksabstimmungen auf Bundesebene nicht stattfinden sollte.

Als Begründung wird herangezogen, daß Volksabstimmungen populistische Tendenzen fördern, die geeignet sein könnten, die
freiheitlich demokratische Grundordnung des deutschen Rechtsstaates aufzuheben.

Bei Betrachtung der Nachrichten über Umfragen im Zusammenhang mit politischen Entscheidungen fällt mir auf, daß diese
Umfragen geradezu geeignet sind, diese Entscheidungen zu rechtfertigen. So nach dem Motto: "Seht her, die Menschen wollen es doch so".

Damit wird ein formal ordentlicher Vorgang wie eine Volksabstimmung durch einen von Umfrageinstituten gemachten Vorgang ersetzt.
