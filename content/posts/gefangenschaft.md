---
title: "Gefangenschaft"
date: 2021-03-24T09:00:08+01:00
draft: false
author: "Sita"
subtitle: ""
image: ""
tags: [erzaehlungen]
---


Ich erinnere mich nicht jemals frei gewesen zu sein.

Mein ganzes Leben war nur von dem Bestreben geprägt mich mit den wenigen Möglichkeiten, die mir gegeben sind, abzufinden.

Ein in Freiheit geborener Mensch würde vielleicht befinden, ich sei in einem Gefängnis geboren, doch das ist nicht, wie ich es nenne, kenne ich doch die Welt nicht anders.
Und jener, der mir sagte, ich sei Gefangene, auch ihm würde es Schwierigkeiten bereiten festzustellen, worin meine Gefangenschaft besteht. Schließlich habe ich keinen Aufseher und keine feste Zelle.

Es ist mir sogar möglich mich mittels eigener Anstrengung um eine andere Arbeit oder einer andere Bleibe zu bemühen.
Von den Dienst habenden Aufsehern werden wir nicht persönlich überprüft, ihr müder Blick ist immer auf alle zugleich gerichtet. Immer leben wir in Gefahr von ihnen für ein uns unbekanntes Verbrechen herausgegriffen, angeklagt und weggesperrt zu werden.
Die wirklichen Verstöße können sie nur durch Zufall herausfinden, denn es gibt zu viele Gefangene um den einzelnen im Blick halten zu können.

Ich weiß nicht einmal ob die Wärter nicht auch Gefangene sind, die eine andere Aufgabe als wir anderen zugewiesen bekommen haben. Beim Überprüfen der Regeln gehen sie ja zufällig vor und genau dieses Vorgehen ist es, daß den Eindruck erweckt, sie wüssten doch besser Bescheid als wir, erweckt den Eindruck sie besitzen Zugang zum Einblick in die Zusammenhänge des Geschehens.
Obwohl es weniger Aufseher als Gefangene gibt, halten wir uns an die Regeln, sie sind die uns gegebene Ordnung, die wir um so mehr zu verlassen fürchten, da wir voneinander nicht wissen, welcher Verbrechen die anderen fähig sind und diese Verbrechen nur durch die Gefangenschaft verhindert werden.

Bricht diese Ordnung weg, wird es gefährlich, das wissen wir.
Denn trotz der Aufsicht und Kontrolle geschehen Morde und Diebstähle und dergleichen mehr.

An meine Kindheit erinnere ich mich kaum.
Aus Erzählungen der Altvorderen, deren Jugend in die Zeit der kurzen Befreiung fiel, hörte ich, daß die Kindheit eine Zeit des unbeschwerten Spiels und Entdeckens war. Entbehrungen hat es zwar mehr als heutigentags gegeben, doch wuren diese in der Lust des Lebens leicht getragen.

Mir sind die schier endlosen, immer gleichen Wege und Straßen in Erinnerung geblieben. Diese hatte ich zurückzulegen, um in die Ausbildungseinrichtung zu gelangen. Noch endloser als die Wege waren die Stunden des stillen Sitzens, in denen wir wir die Sätze und Formeln lernten.

In den Pausen strömten wir, ein großer Kinderpulk in den Pausenhof hinaus – zu den kurzen Momenten des Glücks und der seligen Unbeobachtetheit, denn nur wenige Lehrer beaufsichtigten uns in dieser Zeit, die anderen zogen sich zum Plauschen zurück.
Lehrer, das sind auch die mit gehobenem Posten und gut bezahlt zudem. Für ihren Unterhalt zahlen die Eltern der Kinder, ebenso wie alle Gefangenen für ihre Beaufsichtigung zu zahlen und arbeiten haben.

Rede ich mit anderen Gefangenen über unsere Gefangenenschaft, pflegen sie mich auszulachen. Sie freuen sich über ihre Unfreiheit, heißt es doch, daß wir dank der wohlmeinenden Führung ausreichend Essen wie Arbeit haben. Sie sind froh mit Eintritt in die Gefangenschaft, die schwer lastende Verantwortung der Eigenständigkeit abgegeben zu haben. Sie erinnern sich nur ungern an die Freiheit, schieben diese Gedanken von sich, vergessen in Gefangenschaft geraten zu sein, freuen sich an ihr und preisen sie als die eigentliche Freiheit.

Mich stört es nicht, mich zu erinnern. Auch der Schmerz stört mich nicht.
Es ist mir wohler zu wissen, daß ich eine Gefangene bin, als mir in ständiger Lüge einzureden, ich hätte etwas besonderes erreicht oder einen Glücksstein in der Lotterie gezogen in eben diesem Gefängnis gelandet zu sein.

Auch ich kenne die Bilder der anderen Orte. Es sind Bilder des Grauens. Dort – kümmert sich niemand um die Verteilung von Nahrungsmitteln und Häusern oder Arbeit, dort scheint es, wäre den Menschen das Interesse für ihre Aufsicht zu arbeiten und zu zahlen abhanden gekommen. Und so wählten sie eine gewiße Heftigkeit des Lebens, der Freuden und Verbrechen anstelle der ständigen Unterdrückung.
Ja, ich kenne die Bilder der Toten, Kranken und Hungernden.

Und ich kenne auch den Anblick meiner Mitmenschen, die sich in einer ständigen Hatz auf Wegen befinden, von denen sie selbst nicht wissen wohin sie führen oder woher sie kommen, die versuchen all das, was sie erledigen müssen, in immer kürzerer Zeit getan zu bekommen.

Die in immer größerer Einsamkeit immer enger zusammengepfercht werden. \
Die für immer weniger Vergnügungen immer mehr bereit sind zu arbeiten.

Die Körper und Geist der Forschung und medizinischen Versuchen und abstrusen Ideologien zu verkaufen bereit sind ohne jemals zu fragen.
Und ich frage mich, frage mich, frage mich, ob die Zustände in unserer Gefangenschaft wirklich die besseren sind. Ob es nicht die Gefangenschaft an sich ist, die das Leben so unerträglich mach und unser zusätzliches Leid die gestohlene Ruhe ist.

Doch was soll das Fragen, wenn sich die Übrigen in ihrem Glück besonnt vorkommen, wenn sie sich nicht der Freiheit und Schmerz eines eigenen Lebens zuzuwenden vermögen.

Wer hörte die Klage der wenigen?

Und wer hörte die stumme Klage der vielen, die in fremden Sätzen steckend ihre eigene Klage nicht zu sprechen, noch zu vernehmen in der Lage sind, da sie ihnen allzu fürchterlich klingt.
