---
title: "Die Lücke Des Konstruktivismus"
date: 2021-04-24T05:07:03+02:00
image: ""
draft: true
tags: [philosophie]
---

Der Konstruktivismus hinterläßt eine Lücke, in der der Mensch die wahrnehmbare- und erlebbare Umwelt auf eine rein mechanische 
Funktionsweise der Erscheinung reduziert.

Es ist der Versuch die äußere Welt durch die Vorstellung zu ersetzen.

Dem Menschen wird unterstellt, keinen Zugang zur wahrnehmbaren und erlebbaren Umwelt zu haben, sondern sich 
gleichsam in einem Gefängnis seiner Abbilder der Welt zu bewegen [^1].

Nun ist es unweigerlich so, daß unser Umgang mit der Welt von unseren Ideen geleitet wird.

Sollte ich die Idee eines höheren Wesens haben, das ich nicht sehen und greifen kann, wäre es mir dennoch möglich, 
mich so zu verhalten, als ob es [anwesend wäre](https://mtsusidelines.com/2015/09/14/harvey-play-synopsis/). 

Insofern unsere Ideen handlungsleitend werden, werden sie also Teil der erfahrbaren Umwelt.

Diese gegenseitige Abhängigkeit unseres Erlebens und Handelns von Ideen führt aber nicht dazu, daß alle Wirklichkeit beliebig ist.

Denn die Voraussetzung dafür wäre, daß Ideen keinen Bezug zur Wirklichkeit haben und somit beliebig sind.

Gerade aber diese These zu beweisen ist eine Unmöglichkeit.

Denn dafür müsste der Beweis erbracht werden, daß der Ursprung der Sprache in einer Absicht des Menschen liegt [^2].
Das er im Vorhinein etwas hervorgebracht hat, dessen Wirkung er erst im Nachhinein feststellen kann.

Da der Mensch aber in eine jeweilige Erlebniswelt eingebunden ist, findet er in diesem Eingebundensein den Ursprung seines Wesens.

Dann kann der Mensch keine Ideen ohne Bezug zu einer Wirklichkeit entwickeln.

Nur insofern er sich von dem Ursprung seines Wesen entfernt, ist es ihm möglich die Wirklichkeit durch Abbilder zu ersetzen.

Diese Grenzüberschreitung beinhaltet die Negierung des menschlichen Wesens mit ungewissen Ausgang für dasselbige.

Sollte die Welt nur aus beliebigen, aus der Absicht des Menschen begründeten Dingen bestehen, wird die Möglichkeit allgemeiner
Ideen[^3] ausgeschlossen. Dies ist das, was ich unter der Lücke des Konstruktivismus verstehe.

Obwohl unsere Worte, Ideen, Bilder nur einen Teil der erlebbaren Welt fassen, läßt sich daraus nicht ableiten, 
daß eine für alle Menschen verbindliche Konzeption der Welt nur ohne allgemeine Ideen möglich ist; 
der Mensch in ein Konzept gezwängt wird, in dem er nur noch aus seiner materiellen Beschaffenheit und in der Absicht weniger definierten 
Bedürfnissen handelt [^4].

Entwurf vom 12. April 2021 13:07:03

[^1]: Diese Beschreibung enthält den Zirkelschluss, daß dieses Konzept selbst eine Konstruktion der menschlichen Vernunft sein müsste.
Sollte sie dies nicht sein, wäre es zumindest den Vertretern dieses Konzeptes möglich, einen Zugang zu einer für 
alle Menschen verbindlichen Wirklichkeit zu haben.

[^2]: Hier hilft auch kein Versuch die Sprache als ein reines Zeichensystem aufzufassen. Zwar war dies eine Tendenz der Philosophie 
des 20. Jahrhunderts, doch ist dies eine reine Technizität, die ein bestehendes System auf recht beliebige Kriterien reduziert und 
mithin den Gesamtzusammenhängen nicht gerecht werden kann.

[^3]: Wobei allgmeine Ideen immer etwas Seiendes erfassen und nicht etwa ein auf Übereinkunft basierendes Konzept das intersubjektiv 
geteilt wird.

[^4]: Meine Argumentation umfaßt kurzgefaßt zwei Punkte: 
Zum einen den bereits erwähnten Zirkelschluss [^1], der eher eine logische Spielerei ist. 
Zum anderen den Bezug auf der inhaltlichen Ebene, daß die Idee des Konstruktivismus eine allgemeine Idee einführt, der alle Menschen 
unterworfen sein sollen, ohne das hier eine allgemeine Begründung angegeben werden kann, die sich auf mehr als selektive 
Wahrnehmung und Beschreibung der Welt bezieht. Warum jetzt diesen speziellen Wahrnehmungen eine höhere Wirklichkeit zukommen sollte, 
als anderen widersprechenden Wahrnehmungen und Beschreibungen der Welt, läßt sich nur durch Willkür und Propaganda begründen, 
aber nicht durch einen auf der Sachebene geführten Dialog.
