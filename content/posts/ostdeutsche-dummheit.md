---
title: "Ostdeutsche Dummheit"
date: 2019-09-20T12:15:35+01:00
draft: false
tags: [gesellschaft, politik, eigenart]
---

In der Diskussion um den Erfolg von als Rechts eingeordneten Parteien in 
Ostdeutschland zeigt sich wie vermeintliche Folgerichtigkeiten in unserem
Denken Einzug halten und wir in der Folge unsere Anschauung der Welt nur 
noch bestätigen, anstatt unsere bevorzugten Einstellungen beiseite zu legen
und zu begreifen, was wirklich geschieht.

Eine geläufige Argumentation meint, daß in Ostdeutschland doch so wenige
Flüchtlinge seien und aus diesem Grund die Angst vor einer Überfremdung
völlig unangemessen sei.

Bei dieser Argumentation wird der Begriff des Fremden und der Eigenart als 
kalkulatorische Größe verstanden aber nicht die Bedeutung des Fremden oder
von Eigenart für den Menschen.

Betrachtet man es so, daß Eigenart sich dort am besten entwickeln kann, wo 
sie von fremden Einflüssen weitestgehend unangetastet bleibt, würde ich 
unterstellen, daß die Menschen von ländlichen Regionen in Ostdeutschland
ein wesentlich klareres Empfinden für ihre Eigenart haben, als beispielsweise
ein Bewohner der bundesdeutschen Hauptstadt, dessen Empfindungswelt von 
Einflüssen aus aller Welt geprägt ist und somit kein kultureller Schutzraum
für eine eigene Art von Kultur vorhanden ist. 

Stattdessen wird das alltägliche Treiben aus Versatzstücken unterschiedlichster 
Kulturen zusammengesetzt mit der Folge der fehlenden Identität mit der umgebenden Welt.

Insofern ist dem Großstadtmenschen der Bezug zu einer eigenen Kultur verloren
gegangen. Er findet sich in Versatzstücken unterschiedlichsten Ursprungs. 
So kann er auch keine Angst vor einer Überfremdung haben, da er
dieser bereits vollständig unterliegt. 
Nur dort wo etwas Eigenes aus sich gewachsen ist kann Angst vor dem Fremden entstehen.

Dieser Verlust der kulturellen Integrität des modernen zivilisierten Menschen 
ist als solcher versteckt, da etwas nicht Vorhandenes als solches nicht 
vermißt wird. Nur ähnlich wie bei einer amputierten Extremität, die 
noch einen Phantomschmerz verursachen kann, taucht manchmal im Empfinden
der Eindruck auf, daß etwas fehle. Das führt dann meistens zur großen Suche 
nach dem Sinn des Lebens in meditativer Heilsumgebung des nächsten Yogastudios
oder gleich zur großen Reise in ferne Länder, um dort dem Ursprung dieser 
Unruhe auf die Schliche zu kommen. Das natürlich genau dieses Suchen das 
eigentliche Problem ist und die Ursprungslosigkeit daheim nicht ändert, bleibt
dann den meisten ersteinmal verborgen.
