---
title: "Die Überforderung des Menschen"
date: 2021-04-05T08:55:05+02:00
draft: false
subtitle: ""
image: ""
tags: [gesellschaft]
---

Die Überforderung des Menschen in der Jetztzeit des Jahres 2021 ist überragend.
Die gebräuchlichsten Handlungen und Dinge des Alltags sind in Zusammenhänge eingebunden, die sich der Anschaulichkeit entziehen.

Damit verliert der Mensch seinen Ort und seinen Ursprung.

Ohne Ort und ohne Ursprung bewegt sich der Mensch in einer nur noch mittelbaren Welt, in einer Ordnung und in Zusammenhängen, die
lediglich durch Informationen greifbar werden.

Dabei sind Informationen nichts anderes als die Beschreibung einer Wahrnehmung eines Geschehens außerhalb seines Ortes und seiner Zeit.
