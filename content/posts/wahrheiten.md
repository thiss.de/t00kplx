---
title: "Wahrheiten"
subtitle: ""
date: 2021-08-26T07:23:03+02:00
image: ""
draft: false
tags: []
---

Es gibt sie leider doch: die Wahrheiten, denen kein Mensch entkommt.

Denn was auch immer ein Mensch tut, er wird dadurch in sein Erleben der Welt eingreifen.

Niemand kann ihm diese Folge seines Handelns abnehmen.
