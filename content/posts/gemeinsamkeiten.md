---
title: "Gemeinsamkeiten"
subtitle: ""
date: 2021-10-13T08:18:15+02:00
image: ""
draft: true
tags: []
---

Über meine Mitmenschen weiß ich im wesentlichen ziemlich wenig.

Ich weiß nicht, wie sie sich fühlen, was sie denken, wenn sie über die
Straße gehen oder irgendwas sehen.

Ich weiß nicht, wie sie ihren Abend verbringen, was sie zum Frühstück essen
und über was sie sich Sorgen machen.

Was ich aber sehe ist die geteilte Wirklichkeit.

Das, was alle Menschen in dieser Welt tun müssen: ihre Art des Umgangs
mit dieser Welt finden.
