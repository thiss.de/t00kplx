---
title: "Diese ewigen \"Verschwörungstheorien\""
subtitle: ""
date: 2021-04-24T02:56:37+02:00
image: ""
draft: false
tags: [gesellschaft, philosophie]
---

Alle Theorien, die in einem Geschehen der Vergangenheit die Übel der heutigen Zeit erkennen wollen,
sind nur bedingt [^1] zielführend. 

Denn Probleme sind jetzt in der Welt.

Jeder Versuch ein gegenwärtiges Problem durch einen Fehler der Vergangenheit zu begründen, lenkt von der Möglichkeit ab,
dieses Problem im Jetzt zu lösen.

Denn ein Problem, daß jetzt in der Welt ist, muß auch im Jetzt eine Möglichkeit finden zu wirken.

Somit kann dieses Problem auch durch gegenwärtiges Handeln gelöst werden.

[^1]: Wenn ich hier "bedingt" schreibe, möchte ich die Möglichkeit einer Weltbetrachtung offen lassen, die ohne erlebbaren Gegenwartsbezug
auskommt. Insofern heißt "bedingt" hier nur, daß ich für mich persönlich eine erlebbare Gegenwart brauche in der neben vielen 
anderen Dingen auch die Möglichkeit Platz hat, daß es Menschen gibt, die diese Gegenwart nicht benötigen
