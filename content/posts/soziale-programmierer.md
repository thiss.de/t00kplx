---
title: "Soziale Programmierer"
date: 2019-12-27T07:38:49+01:00
draft: false
subtitle: ""
image: ""
tags: [softwareentwicklung]
---

Es gibt ja diese unterschiedlichen Arten Software zu schreiben. Da kann man an der Universität
oder in Büchern sehr viel lernen, wie gute bis nahezu perfekte Software geschrieben wird.
Irgendwie kommen wir da in den Bereich der "reinen Softwareentwicklung", die sich vor allem
um Fehlerfreiheit und klare Beschreibung eines Vorgangs bezieht, einen klaren Satz von Anweisungen.

So dürften auch die meisten der heutigen Programmierer, vor allem die jungen, ihre Ausbildung in
der Universität oder Fachhochschule abgeschlossen haben und somit im klaren akademischen Bereich
das Erstellen von Software gelernt haben.
Eine gewisse Praxisferne gehört dazu, damit die Aufmerksamkeit auf das Ideal nicht durch Erwägungen
des realen Lebens gestört wird.
Selbstverständlich werden heutzutage auch praktische Aufgaben, Praxissemester und Werkstudententätigkeit
verlangt, aber das hat dann mit der Welt auch nur soviel zu tun, wie eine Demo mit dem vollständigen
Produkt.

Hier zeigt sich schon, daß es irgendeinen Unterschied gibt, zwischen dem ideal zu Erlernendem und dem
praktisch Auszuübendem.

Nun, wenn nach der Ausbildung die erste Arbeitsstelle angetreten wird, ergeben sich andere Probleme,
als die der reinen Softwareentwicklung. Denn ist man im akademischen Bereich noch frei, die beste und
effektiveste Lösung für ein Problem umzusetzen, muss die Lösung nun auch für alle anderen im Team
nachvollziehbar sein. Und dann kann es halt vorkommen, daß man aufgrund seiner Einsicht, eine bestimmte
Programmiersprache für eine Lösung nutzen möchte, weil es sich vielleicht die Lösung funktional am besten
beschreiben ließe und deshalb in Scala, Clojure, Erlang oder etwa Lisp umsetzen möchte.
Aber dann erhebt sich ein Murren in der Herde, die Lösung sei zu kompliziert, nicht verständlich, könne
man genausogut in Java, Python, C umsetzen und das kenne auch jeder andere im Team.

Nun, es wird seinen Grund haben, warum man diese Implementierung gewählt hat oder diese für am besten hält.
Das Phänomen mit dem man es hier zu tun hat, ist nämlich keines der besten oder reinsten Software, es ist
ein Herdenproblem.

Es gibt ja so und soviele Abteilungen in Unternehmen die Software schreiben und am
Ende des Tages ist die Arbeit für viele nur ein Teil ihres Lebens. Nach der ersten Entscheidung in die
IT-Industrie zu gehen, treten andere Dinge ins Leben und das abstrakte Denken in Funktionen tritt in den
Hintergrund. Es ist halt nicht jeder ein geborener Entwickler, sondern manchmal sind es nur die guten
Verdienstmöglichkeiten, die dann dazu führen, daß jemand mit verquälter Eigenart bis zum Ende seines
Berufslebens in Java Webanwendungen schreibt.

In so einer Herde sind die Maßstäbe verschoben. Es geht nicht mehr um die Sache, sondern um den Zusammenhalt.
Wenn der Zusammenhalt im Vordergrund steht, kann selbstverständlich nicht die beste Lösung umgesetzt werden.

Hier hilft nur die Auslöschung der Herdenstruktur und die Bereitschaft aus eigenständigen Entwicklern ein
Team aufzubauen, in dem jeder seine speziellen Möglichkeiten und Anlagen umsetzen kann.
Dabei sollte die Sprache, also die Programmiersprache nicht der gemeinsame Nenner sein, sondern Mathematik,
Pseudo-Code, Tests und Dokumentation. Denn letztlich ist ein gutes Design in einer "fremden" Programmiersprache
allemal verständlicher, als ein schlecht designtes Programm in einer bekannten Programmiersprache.

Das wäre dann als Grundlage für ein Team, eine Herausforderung, so etwas aufzubauen.

Nicht die gemeinsame Programmiersprache, die gemeinsamen Bibliotheken, die gemeinsamen Frameworks, sondern
der gemeinsame Anspruch, der gemeinsame Umgang mit der Arbeit.
