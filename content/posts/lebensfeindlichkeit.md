---
title: "Die Sünde der Ideologen"
subtitle: ""
date: 2021-06-13T08:53:38+02:00
image: ""
draft: true
tags: [gesellschaft, philosophie]
---

Aus den Handlungsmöglichkeiten des Menschen wählen Ideologen aus.

Sie bilden ein Modell des Menschen, ohne Bezug zu seinem Dasein: 
abstrakt, ort- und zeitlos.

Dann zwingen sie andere Menschen in diese Ausübung des Menschlichen 
und rauben die Freiheit, nehmen dem Menschen die Möglichkeit mit der 
Welt auf eine eigene Art umzugehen.
