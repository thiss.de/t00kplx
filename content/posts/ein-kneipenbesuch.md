---
title: "Ein Kneipenbesuch"
date: 2019-07-07T12:30:05+01:00
draft: false
tags: [gesellschaft, philosophie, erkenntnistheorie]
---

Gestern Abend hatte ich das Vergnügen mit meinem Sohn einer kleinen Kneipe
einen Besuch zu erstatten. Mein Sohn kann leicht Kontakt mit anderen Menschen
knüpfen und so ergab sich ein Gespräch mit zwei jungen Frauen, in deren Verlauf
er auf einige Fehler am gegenwärtigen Gesellschaftssystem hinwies.
Das Gespräch wurde an der Stelle für mich interessant, als eine der beiden
darum bat, daß er doch auch erklären solle, was man denn Richtiges tun soll, da
seine bisherigen Aussagen lediglich seine Meinungen darstellten.

An dieser Stelle konnte ich mich nicht zurückhalten in das Gespräch
einzusteigen.

Zuerst wollte ich von ihr erklärt bekommen, inwiefern es notwendig sei, neben
dem Verweis auf die Falschheit eines Tuns ein alternatives Tun zu verlangen,
da es doch für jemanden der etwas Falsches tut schon eine hinreichende
Hilfestellung sei dies einzusehen und er mit dieser Erkenntnis einen Weg
suchen kann, sein Ziel ohne falsches Tun zu erreichen und es eben nicht
notwendig ist, ihm ein anderes Ziel zu geben. Dies wäre natürlich tatsächlich
dann nicht hinreichend, wenn demjenigen, der etwas Falsches tut, die
Falschheit seines Tuns schon vorab bekannt wäre.

Zu diesem Punkte wollte sie sich nicht weiter äußern, da es eben ihre Meinung
sei, daß sie nicht auf das Falsche ihres Tuns hingewiesen werden möchte, wenn
ihr niemand sagt, was sie stattdessen tun solle.

Dann bat ich sie doch darum mir den Begriff der Meinung etwas zu erläutern,
denn ich mag es, die Begriffe des Denkens meines Gegenübers genauer zu
verstehen. Die Beschreibung war nicht sonderlich klar, zusammenfassend klang es
so, als ob sie sagen wollte, daß eine Meinung eine aus dem Empfinden
geäußerte Beschreibung einer Ansicht der Welt sei. In ihren Worten, eine aus
dem subjektiven mit Ablehnung oder Zustimmung geäußerte Wahrnehmung der Welt
und auf alle Fälle sei eine Meinung keine Tatsache. An dieser Stelle hätte
ich gerne die Wahrnehmung der Welt und das darin Wahrnembare weiter
besprochen, doch zog es mich erstmal zu dem Stichwort *Tatsache* und so
wollte ich wissen, was denn eine Tatsache sei.

Eine Tatsache ist also, eine Beschreibung eines Zustandes der Welt in dem
viele Menschen übereinstimmen, wie zum Beispiel, daß die Erde eine runde
Form habe und sich die Erde um die Sonne drehe und nicht etwas andersherum.
Nach der vorherigen Definition von Meinung ließ sich an dieser Stelle bereits
folgern, daß eine Tatsache die Meinung von vielen sei, so wollte ich also
wissen, ob es denn dann auch eine Tatsache sein könne, daß die Erde tatsächlich
flach ist, wenn ein soziales System von Menschen darin übereinstimmte.

Hier hatte ich jetzt erwartet, daß ihr die Lücke in ihrer Argumentation auch
auffallen müsse, aber ich sei mich zu meinem Erstaunen getäuscht.
Sie erklärte wirklich, daß es dann eine Tatsache sei, das die Erde flach wäre.

Hier zeigt sich die Unfähigkeit einen Gedanken zu Ende denken zu können,
die Unfähigkeit eine Welt außerhalb der eigenen Nützlichkeit zu verstehen.

Es gibt noch eine [Ergänzung]({{< relref "ergaenzung-kneipenbesuch" >}}) hierzu.
