---
title: "Meinung"
subtitle: ""
date: 2021-04-18T05:28:03+02:00
image: ""
draft: false
tags: [philosophie]
---

Was soll diese "Meinung" sein, die alles ist, was man selber sieht?

Mittlerweile scheint es mir, es ist der Kampfbegriff derjenigen geworden, die das Erleben der anderen Menschen 
mit ihren Vorstellungen besetzen wollen.

Offensichtlich akzeptieren die meisten Menschen die Existenz von Sätzen, die Nicht-Meinung sind.

Außerdem kann man das Meinungsspiel wunderbar bis ins Unendliche weiterspielen.

A: "Das (bezieht sich auf irgendeine Aussage über die Welt) ist nur deine Meinung" \
B: Wie kommst du darauf?" \
A: "Es gibt Experten, die anderer Ansicht sind." \
B: "Woher weißt du, daß das Experten sind." \
A: "Ich bin der Meinung, daß das Experten sind." \
A: "Wie kommst du zu dieser Meinung?" \
B: "Viele Leute meiner Umgebung, sagen das." \
A: "Und das soll ein Grund sein, diese Meinung zu übernehmen?" \
B: "Ja, da bin ich schon der Meinung." \
...

Dieser Zirkel der Meinungsmeinerei wird nur durch das Erleben durchbrochen.

Solange wir auf den Gleisen stehen und nur die Meinung austauschen, ob ein Zug auf uns zurast oder nicht,
solange werden wir darüber streiten können.

Aber wenn der Zug nahe genug ist, daß wir ihn eindeutig wahrnehmen, dann ist Schluß mit Meinungsmeinerei, 
dann springen wir hoffentlich von den Gleisen (sofern wir nicht anfangen, darüber zu diskutieren, ob es jetzt nur eine Meinung ist, 
  daß einem der Sprung von den Gleisen rettet oder nicht, oder es vielleicht besser wäre auf den Gleisen zu verbleiben...).
