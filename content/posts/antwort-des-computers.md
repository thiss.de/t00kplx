---
title: "Antwort Des Computers"
subtitle: "Die Kunst zu Programmieren"
date: 2019-11-12T12:15:48+01:00
image: ""
draft: false
tags: [programmieren, gesellschaft, softwareentwicklung]
---

Es ist etwas irritierend über so etwas banales wie Computerprogramme zu reden.
Das sind lediglich eine Abfolge von Anweisungen, die angeben wie ein wohlverstandenes
Problem wiederholt mit wechselnden Werten gelöst werden kann.

Geistloser und einfallsloser kann es fast nicht mehr werden.

Nicht etwa, weil es nicht großer Denkanstrengung bedürfte, diese Programme zu schreiben.
Dabei handelt es sich tatsächlich um eine geistvolle Tätigkeit
die viel abstraktes Denkvermögen und Wissen verlangt.

Es ist die Einschränkung und der Anspruch sich nur Problemen zu widmen,
denen immer wieder das gleiche Muster zugrundeliegt und deshalb
durch ein Programm gelöst werden können.

Diese Klasse von Problemen verlangt nach einer Einschränkung der Welt.

Neuronale Netze funktionieren noch anders. Sie erlauben das Muster anhand von Daten und einer
gewünschten Lösung selber zu entdecken, so daß es nicht mehr notwendig ist, das Muster, den
Algorithmus selber zu entdecken.
Stattdessen wird das Neuronale Netz dieses Muster entdecken und in der Folge bei wechselnden
Eingangswerten eine entsprechende Antwort bieten.

Die geistige Leistung besteht dann darin eine möglichst effiziente Möglichkeit zu finden,
wie das neuronale Netz aus den Daten lernen soll.
Wobei es bis vor einiger Zeit hieß, dies sei mehr eine Kunst aus Versuch und Irrtum erlernt,
als eine exakte Wissenschaft.

Welche Aspekte der Daten genau dafür zuständig sind, wie die Welt beschaffen sein muß, damit
diese Antwort möglich wird und zutreffend ist, bleibt dem Fragenden verborgen.
Was das Bewirkende in der wahrnehmbaren Welt für diese Antwort war, erschließt sich nicht.

Es findet die Trennung statt, daß Antworten gegeben werden, denen man folgen muß, aber
kein Bezug besteht, zu den Gründen und der zugrundeliegenden Wirklichkeit.

Die Situation scheint absurd, daß nicht mehr der Programmierer den Computer anweist, bestimmte
Dinge zu tun, sondern der Computer dem Menschen Anweisungen gibt, was er zu tun habe.
