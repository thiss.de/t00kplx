---
title: "Sätze über die Welt"
subtitle: ""
date: 2021-03-15T08:26:48+01:00
image: ""
draft: false
tags: [gesellschaft, bildung, philosophie]
---

Ist "die Wissenschaft" die einzige Instanz, die gültige Sätze über die Welt äußern darf?

Wollen wir uns als Gesellschaft darauf einigen, daß wir keinen Satz als wahr annehmen dürfen,
der nicht von einem Menschen geäußert wurde, der nicht als Vertreter "der Wissenschaft" gilt?
