---
title: "Der Unsinnige Glaube an Technik"
date: 2019-12-18T09:42:56+01:00
draft: false
subtitle: ""
image: ""
tags: [technik, philosophie]
---

Ich habe es immer wieder erlebt: ein Kunde möchte ein System aufsetzen um
seine Daten auswerten zu können. Ich habe dann versucht herauszufinden, mit welchen
Schritten, welcher Abfolge er die Daten zusammenführen, aggregieren,
übersetzen, etc. will. Diese Fragestellung war dann häufig zu ermüdend und
er meinte, es gäbe doch da jetzt eine neue Software, Hardware, Service, etc.
die sicherlich alles könnten, was er brauche.

Als ob Technologie jemals ein Problem gelöst hätte, für das sie nicht entworfen wurde.

Worauf ich hinaus möchte ist, daß es keinen Zweck hat sich eine Technologie
auszuwählen, wenn man nicht begriffen, hat, was man eigentlich tun möchte.

Sich loszulösen, von dem was man meint tun zu können, von den großen Plänen, die
man vielleicht hat, diese erstmal losgelöst von einer konkret fassbaren Lösung
für sich stehen zu lassen.

Denn Bewußtsein entsteht erst, wenn man die Dinge für sich betrachtet, ohne
die subjektive Befangenheit der eigenen Fähigkeiten.

Das Erschaffen läßt sich nicht auf einen reproduzierbaren Vorgang reduzieren.
Das würde zwar das Scheitern ausschließen, aber auch die Möglichkeit für einen
großartigen Erfolg.

Es geht eben darum, den gesamten Prozeß anzuschauen, der umgesetzt werden soll.

Mit einer Bewußtheit über das, was prinzipiell geschieht an die Projekte herangehen
und eine Strategie entwickeln, wie das gesetzte Ziel erreicht werden kann.
