---
title: "Ideen Des Abendlandes"
subtitle: ""
date: 2021-05-15T11:55:29+02:00
image: ""
draft: false
tags: [philosophie]
---

Einige Ideen der abendländischen Kultur:

-  Begriffe, die allgemeines bezeichnen, sind eine Herleitung aus einzelnen Sinneseindrücken ([Nominalismus](https://de.wikipedia.org/wiki/Universalienproblem#Starker_Nominalismus))
-  Begriffe, die allgemeines bezeichnen, sind **keine** Herleitung aus einzelnen Sinneseindrücken ([Realismus](https://de.wikipedia.org/wiki/Universalienproblem#Starker_Realismus))
-  Beobachtungen sollten mit sowenig Begriffen wie möglich erklärt werden ([Occams Rasiermesser](https://de.wikipedia.org/wiki/Ockhams_Rasiermesser)).
-  Wahr sind Sätze über die Welt, die nützlich sind [^1].


[^1]: Soweit mir bekannt, wird diese Auffassung auf [Francis Bacon](https://de.wikipedia.org/wiki/Francis_Bacon) zurückgeführt.
