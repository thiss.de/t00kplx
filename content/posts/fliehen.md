---
title: "Fliehen"
subtitle: ""
date: 2021-10-13T06:38:12+02:00
image: ""
draft: false
tags: [gesellschaft]
---

In letzter Zeit höre ich bei vielen das Verlangen zu fliehen aus dieser Welt,
aus dieser westlichen Industriegesellschaft.

Das halte ich auf der einen Seite für sehr verständlich, wenn ich die möglichen
Daseinszusammenhänge betrachte, die ein Mensch in diesem Teil der Erde noch
leben kann.

Auf der anderen Seite halte ich es für vergebliches Tun, die Flucht anzutreten.

Denn wir Menschen der Industriegesellschaft, sind es gewohnt, durch
einfachen Vollzug unsere Daseinszusammenhänge zu ändern. Wir "machen" einfach
irgendwas und dann soll es anders sein.

Aber dieses einfache "Machen", entspricht nicht dem menschlichen Wesen
(und treibt uns eigentlich aus den Daseinszusammenhängen des Menschen heraus).

Einfaches "Machen" reduziert uns auf das Körperliche, auf den nur sichtbaren
Teil der menschlichen Lebensäußerungen.

Es wäre viel gewonnen, wenn der "Schein der Welt", wieder den Platz im
Bewußtsein einnähme, der ihm gebührt. Nämlich den, des lediglich sichtbaren
Teils eines Ausdrucks von Erleben.
